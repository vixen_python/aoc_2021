import numpy

data = open('aoc6_input.txt', 'r').read().split(',')
int_data = [int(x) for x in data]
int_data2 = [3, 4, 3, 1, 2]


def lantern_fish(days, initial):
    fish_school = initial
    for _ in range(days):
        for i, fish in enumerate(fish_school):
            if fish == 0:
                fish_school[i] = 6
                fish_school.append(9)
            else:
                fish_school[i] -= 1
    return len(fish_school)


def lantern_fish_huge(days, initial):
    stage = [0 for _ in range(9)]
    for internal_timer in initial:
        stage[internal_timer] += 1

    for day in range(days):
        today = day % len(stage)

        stage[(today + 7) % len(stage)] += stage[today]

    return sum(stage)


print(lantern_fish(80, int_data))
int_data = [int(x) for x in data]
print(lantern_fish_huge(256, int_data))

