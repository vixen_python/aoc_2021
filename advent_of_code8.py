data = open('aoc8_input.txt', 'r').read().split('\n')
output_values_rows = [x.split("|")[1].strip() for x in data]
input_values_rows = [x.split("|")[0].strip() for x in data]
output_values = [x.split(" ") for x in output_values_rows]
input_values = [x.split(" ") for x in input_values_rows]
output_values_flat = [item for sublist in output_values for item in sublist]


def only_digits(values):
    result = 0
    for value in values:
        if len(value) == 2 or len(value) == 3 or len(value) == 4 or len(value) == 7:
            result += 1

    return result


def position1(pattern):
    seven = filter(lambda x: len(x) == 3, pattern)
    seven = set(list(list(seven)[0]))
    one = filter(lambda x: len(x) == 2, pattern)
    one = set(list(list(one)[0]))
    return seven.symmetric_difference(one).pop()


def position_by_count(pattern, desired_count):
    key = "abcdefg"
    together = "".join(pattern)
    for letter in key:
        if together.count(letter) == desired_count:
            return letter


def position3(pattern, value):
    one = list(filter(lambda x: len(x) == 2, pattern))[0]
    for character in one:
        if character != value:
            return character


def position4(pattern, val1, val2, val3):
    four = list(filter(lambda x: len(x) == 4, pattern))[0]
    for character in four:
        if character != val1 and character != val2 and character != val3:
            return character


def cipher(pattern, output):
    result = 0

    for i in range(len(pattern)):
        positions = {}
        positions["pos1"] = position1(pattern[i])
        positions["pos2"] = position_by_count(pattern[i], 6)
        positions["pos5"] = position_by_count(pattern[i], 4)
        positions["pos6"] = position_by_count(pattern[i], 9)
        positions["pos3"] = position3(pattern[i], positions["pos6"])
        positions["pos4"] = position4(pattern[i], positions["pos2"], positions["pos3"], positions["pos6"])
        positions["pos7"] = set(positions.values()).symmetric_difference(set('abcdefg')).pop()

        numbers = [
            {positions["pos1"], positions["pos2"], positions["pos3"], positions["pos5"], positions["pos6"], positions["pos7"]}, # noqa
            {positions["pos3"], positions["pos6"]},
            {positions["pos1"], positions["pos3"], positions["pos4"], positions["pos5"], positions["pos7"]},
            {positions["pos1"], positions["pos3"], positions["pos4"], positions["pos6"], positions["pos7"]},
            {positions["pos2"], positions["pos3"], positions["pos4"], positions["pos6"]},
            {positions["pos1"], positions["pos2"], positions["pos4"], positions["pos6"], positions["pos7"]},
            {positions["pos1"], positions["pos2"], positions["pos4"], positions["pos5"], positions["pos6"], positions["pos7"]},  # noqa
            {positions["pos1"], positions["pos3"], positions["pos6"]},
            {positions["pos1"], positions["pos2"], positions["pos3"], positions["pos4"], positions["pos5"], positions["pos6"], positions["pos7"]},  # noqa
            {positions["pos1"], positions["pos2"], positions["pos3"], positions["pos4"], positions["pos6"], positions["pos7"]},  # noqa
        ]

        partial = []
        for value in output[i]:
            for number, code in enumerate(numbers):
                if code == set(list(value)):
                    partial.append(str(number))
        result += int("".join(partial))
    return result


print(only_digits(output_values_flat))
print(cipher(input_values, output_values))
