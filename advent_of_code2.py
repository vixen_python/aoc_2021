data = open('aoc2_input.txt', 'r').read().split('\n')


def multiply(data):
    horizontal = 0
    depth = 0
    for instruction in data:
        direction = instruction.split()[0]
        value = int(instruction.split()[1])
        if direction == "forward":
            horizontal += value
        elif direction == "down":
            depth += value
        elif direction == "up":
            depth -= value
    return horizontal * depth


def multiply_with_aim(data):
    horizontal = 0
    depth = 0
    aim = 0
    for instruction in data:
        direction = instruction.split()[0]
        value = int(instruction.split()[1])
        if direction == "down":
            aim += value
        elif direction == "up":
            aim -= value
        elif direction == "forward":
            horizontal += value
            depth += aim * value
    return horizontal * depth


print(multiply(data))
print(multiply_with_aim(data))
