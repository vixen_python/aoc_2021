drawn, *tables = open('aoc4_input.txt', 'r').read().split('\n\n')
drawn = drawn.split(',')
tables = [[lines.split() for lines in table.split('\n')] for table in tables]
marked = [[[False for number in range(5)] for line in range(5)] for table in tables]


def is_winner(marked):
    for i, table in enumerate(marked):
        transpose = zip(*table)
        for line in table:
            if all(line):
                return i+1
        for line in transpose:
            if all(line):
                return i+1
    return 0


def is_winner2(marked):
    result = []
    for i, table in enumerate(marked):
        transpose = zip(*table)
        for line in table:
            if all(line):
                result.append(i)
        for line in transpose:
            if all(line):
                if i not in result:
                    result.append(i)
    return result


def unmarked_numbers(index, tables, marked):
    result = 0
    for i, line in enumerate(marked[index]):
        for j, value in enumerate(line):
            if not value:
                result += int(tables[index][i][j])
    return result


def draw(drawn, tables, marked):
    for draw in drawn:
        for i, table in enumerate(tables):
            for j, line in enumerate(table):
                for k, number in enumerate(line):
                    if number == draw:
                        marked[i][j][k] = True
        if (winning:= is_winner(marked)):
            unmarked = unmarked_numbers(winning-1, tables, marked)
            return int(draw) * unmarked


def last_to_win(drawn, tables, marked):
    for draw in drawn:
        for i, table in enumerate(tables):
            for j, line in enumerate(table):
                for k, number in enumerate(line):
                    if number == draw:
                        marked[i][j][k] = True
        if (winning:= is_winner2(marked)):
            if len(tables) == 1:
                unmarked = unmarked_numbers(0, tables, marked)
                return int(draw) * unmarked
            for table in reversed(winning):
                tables.pop(table)
                marked.pop(table)


print(draw(drawn, tables, marked))
marked = [[[False for number in range(5)] for line in range(5)] for table in tables]
print(last_to_win(drawn, tables, marked))

