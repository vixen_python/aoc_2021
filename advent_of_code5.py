data = open('aoc5_input.txt', 'r').read().split('\n')
coordinates_in = [line.split(' -> ')[0] for line in data]
coordinates_out = [line.split(' -> ')[1] for line in data]
coordinates_x1 = [line.split(',')[0] for line in coordinates_in]
coordinates_y1 = [line.split(',')[1] for line in coordinates_in]
coordinates_x2 = [line.split(',')[0] for line in coordinates_out]
coordinates_y2 = [line.split(',')[1] for line in coordinates_out]
length = len(data)


def table(x, y):
    return [[0 for position in range(int(x)+1)] for line in range(int(y)+1)]


def overlap(x1, y1, x2, y2):

    for index in range(length-1, -1, -1):
        if x1[index] != x2[index] and y1[index] != y2[index]:
            x1.pop(index)
            x2.pop(index)
            y1.pop(index)
            y2.pop(index)

    max_x = max(x1) if max(x1) > max(x2) else max(x2)
    max_y = max(y1) if max(y1) > max(y2) else max(y2)
    board = table(max_x, max_y)

    for instruction in range(len(x1)):
        if x1[instruction] == x2[instruction]:
            smaller = int(min(y1[instruction], y2[instruction]))
            bigger = int(max(y1[instruction], y2[instruction])) + 1
            line = [f'{x1[instruction]},{y}' for y in range(smaller, bigger)]
        elif y1[instruction] == y2[instruction]:
            smaller = int(min(x1[instruction], x2[instruction]))
            bigger = int(max(x1[instruction], x2[instruction])) + 1
            line = [f'{x},{y1[instruction]}' for x in range(smaller, bigger)]
        for point in line:
            board[int(point.split(',')[1])][int(point.split(',')[0])] += 1

    result = 0
    for row in board:
        result += sum(1 for i in row if i > 1)
    return result


print(overlap(coordinates_x1, coordinates_y1, coordinates_x2, coordinates_y2))
