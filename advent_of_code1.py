def depth_increased(data):
    result = 0
    depth = [int(x) for x in data]
    previous = 0
    for number in depth:
        if number > previous:
            result += 1
        previous = number
    return result - 1


def depth_of_three_increased(data):
    result = 0
    depth = []
    while len(data)>2:
        depth.append(int(data[0])+int(data[1])+int(data[2]))
        data = data[1:]
    previous = 0
    for number in depth:
        if number > previous:
            result += 1
        previous = number
    return result - 1


data = open('aoc1_input.txt', 'r').read().split('\n')

print(len(data))
print(depth_increased(data))
print(depth_of_three_increased(data))
