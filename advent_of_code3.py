from statistics import mode

data = open('aoc3_input.txt', 'r').read().split('\n')
data2 = ['00100', '11110', '10110', '10111', '10101', '01111', '00111', '11100', '10000', '11001', '00010', '01010']


def power_consumption(data):
    gamma = ''
    epsilon = ''
    transpose = list(zip(*data))
    for numbers in transpose:
        if numbers.count('1') > len(numbers)/2:
            gamma += '1'
            epsilon += '0'
        else:
            gamma += '0'
            epsilon += '1'
    gamma = int(gamma, 2)
    epsilon = int(epsilon, 2)
    return gamma * epsilon


def life_support_rating(values):
    length = len(values[0])
    data = values

    for index in range(length):
        transpose = list(zip(*data))
        most_common = mode(sorted(transpose[index], reverse=True))
        data = [number for number in data if number[index] == most_common]
        if len(data) == 1:
            oxygen = data[0]
            break

    data = values
    for index in range(length):
        transpose = list(zip(*data))
        least_common = '0' if mode(sorted(transpose[index], reverse=True)) == '1' else '1'
        data = [number for number in data if number[index] == least_common]
        if len(data) == 1:
            co2 = data[0]
            break

    oxygen = int(oxygen, 2)
    co2 = int(co2, 2)
    return oxygen * co2


print(power_consumption(data))
print(life_support_rating(data))
print(life_support_rating(data2))



