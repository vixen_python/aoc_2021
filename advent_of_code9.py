data = open('aoc9_input.txt', 'r').read().split('\n')
list_data = [list(x) for x in data]


def low_points(data):
    vertical_max = len(data) - 1
    horizontal_max = len(data[0]) - 1
    result = 0
    for i, row in enumerate(data):
        for j, value in enumerate(row):
            if i == 0:
                if j == 0:
                    if data[i][j+1] > value and data[i+1][j] > value:
                        result += 1 + int(value)
                elif j == horizontal_max:
                    if data[i][j-1] > value and data[i+1][j] > value:
                        result += 1 + int(value)
                else:
                    if data[i][j-1] > value and data[i][j+1] > value and data[i+1][j] > value:
                        result += 1 + int(value)
            elif i == vertical_max:
                if j == 0:
                    if data[i][j+1] > value and data[i-1][j] > value:
                        result += 1 + int(value)
                elif j == horizontal_max:
                    if data[i][j-1] > value and data[i-1][j] > value:
                        result += 1 + int(value)
                else:
                    if data[i][j-1] > value and data[i][j+1] > value and data[i-1][j] > value:
                        result += 1 + int(value)
            else:
                if j == 0:
                    if data[i][j+1] > value and data[i+1][j] > value and data[i-1][j] > value:
                        result += 1 + int(value)
                elif j == horizontal_max:
                    if data[i][j-1] > value and data[i+1][j] > value and data[i-1][j] > value:
                        result += 1 + int(value)
                else:
                    if data[i][j-1] > value and data[i][j+1] > value and data[i+1][j] > value and data[i-1][j] > value:
                        result += 1 + int(value)
    return result


print(low_points(list_data))
