
data = open('aoc7_input.txt', 'r').read().split(',')
position = [int(x) for x in data]
position2 = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]


def fuel_cost(positions):
    maximum = max(positions)
    result = 0
    for i in range(maximum):
        costs = [0 for _ in range(maximum)]
        for index, position in enumerate(positions):
            costs[index] = abs(position - i)
        if result == 0:
            result = sum(costs)
        elif result > sum(costs):
            result = sum(costs)
    return result


def fuel_expensive(positions):
    average = round(sum(positions)/len(positions)) - 1
    costs = [0 for _ in range(max(positions))]
    for index, position in enumerate(positions):
        costs[index] = sum([x+1 for x in range(abs(position - average))])
    return sum(costs)


print(fuel_cost(position))
position = [int(x) for x in data]
print(fuel_expensive(position))
